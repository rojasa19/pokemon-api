## Pokemon finder - Backend

- Todo el proyecto Typescript/express se encuentra en el carpeta **src**
- El build del mismo se cuentra en la carpeta **dist**.

**Plus:**

- Se genero un Dockerfile para montar una imagen con la api.
- La misma se subio a al servicio AWS ECR, donde se guarda la ultima versión de la api.
- Esto se levanta automaticamente en un una TASK del servicio AWS ECS.
- Por el tiempo no llegue a levantar un balanceador.
- En el archivo package.json: `npm run deploy` hace el proceso de build, subida del docker y restar de la api en ecs.