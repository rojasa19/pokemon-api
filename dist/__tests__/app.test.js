"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const index_1 = __importDefault(require("../index"));
const supertest_1 = __importDefault(require("supertest"));
describe("Pruebas de la API LIST POKEMON", () => {
    it('Response OK: La api debe existir y responder 200', () => __awaiter(void 0, void 0, void 0, function* () {
        const pokemonName = "mew";
        const pokemons = yield supertest_1.default(index_1.default).get(`/${pokemonName}/list`);
        expect(pokemons.status).toEqual(200);
    }));
    it('LIST no vacia: La api debe devolver mas de un pokemon', () => __awaiter(void 0, void 0, void 0, function* () {
        const pokemonName = "mew";
        const pokemons = yield supertest_1.default(index_1.default).get(`/${pokemonName}/list`);
        expect(pokemons.body.lenth).not.toBe(0);
    }));
    it('LIST vacia: La api no debe devolver pokemons', () => __awaiter(void 0, void 0, void 0, function* () {
        const pokemonName = "pepe";
        const pokemons = yield supertest_1.default(index_1.default).get(`/${pokemonName}/list`);
        expect(pokemons.body).toEqual([]);
    }));
});
describe("Pruebas de la API POKEMON BY NAME", () => {
    it('Response OK: La api debe existir y responder 200', () => __awaiter(void 0, void 0, void 0, function* () {
        const pokemonName = "pikachu";
        const pokemons = yield supertest_1.default(index_1.default).get(`/${pokemonName}/data`);
        expect(pokemons.status).toEqual(200);
    }));
    it('Imagen default: La api debe devolver la imagen front del pokemon', () => __awaiter(void 0, void 0, void 0, function* () {
        const pokemonName = "pikachu";
        const pokemons = yield supertest_1.default(index_1.default).get(`/${pokemonName}/data`);
        expect(pokemons.body.sprites.front_default).toEqual("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/25.png");
    }));
});
//# sourceMappingURL=app.test.js.map