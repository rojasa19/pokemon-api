"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PokemonController = void 0;
const fetch = require('node-fetch');
class PokemonController {
    constructor() {
        this._url = 'https://pokeapi.co/api/v2';
        this._pokemons = [];
    }
    init() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let response = yield fetch(`${this._url}/pokemon/?offset=0&limit=964`);
                let pokemons = yield response.json();
                this._pokemons = pokemons.results;
            }
            catch (err) {
                console.log(err);
            }
        });
    }
    pokemonsByName(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (!req.params.name) {
                    return res.status(404).send("El nombre del pokemon es obligatorio");
                }
                let pokemonsFilter = this.filterByName(req.params.name);
                return res.send(pokemonsFilter);
            }
            catch (err) {
                return res.status(404).send(err.message);
            }
        });
    }
    getPokemonData(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (!req.params.name) {
                    return res.status(404).send("El nombre del pokemon es obligatorio");
                }
                let pokemon = yield fetch(`${this._url}/pokemon-form/${req.params.name}`);
                let data = yield pokemon.json();
                return res.send(data);
            }
            catch (err) {
                return res.status(404).send(err.message);
            }
        });
    }
    filterByName(name) {
        return this._pokemons.filter((pokemon) => pokemon.name.toLowerCase().includes(name.toLowerCase()));
    }
}
exports.PokemonController = PokemonController;
//# sourceMappingURL=pokemons.controller.js.map