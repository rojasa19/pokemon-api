import express, {Application} from 'express';
import router from './routes/index';
import cors from 'cors';
const app: Application = express();

const options:cors.CorsOptions = {
  allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "X-Access-Token"],
  credentials: false,
  methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
  origin: ["http://localhost:3000", "http://ddm3hornmjgee.cloudfront.net/"],
  preflightContinue: false
};

app.use(cors(options));
app.use(router);

export default app;