import {Router} from 'express';
import {PokemonController} from '../controller/pokemons.controller';
const router: Router = Router();
const pokemon = new PokemonController();
pokemon.init();

router.get('/:name/list', pokemon.pokemonsByName.bind(pokemon));
router.get('/:name/data', pokemon.getPokemonData.bind(pokemon));

export default router;