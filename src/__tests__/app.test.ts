import server  from "../index";
import request from "supertest";

describe("Pruebas de la API LIST POKEMON", () => {
    it('Response OK: La api debe existir y responder 200', async () => {
        const pokemonName = "mew";
        const pokemons = await request(server).get(`/${pokemonName}/list`);
        expect(pokemons.status).toEqual(200);
    });

    it('LIST no vacia: La api debe devolver mas de un pokemon', async () => {
        const pokemonName = "mew";
        const pokemons = await request(server).get(`/${pokemonName}/list`);
        expect(pokemons.body.lenth).not.toBe(0);
    });

    it('LIST vacia: La api no debe devolver pokemons', async () => {
        const pokemonName = "pepe";
        const pokemons = await request(server).get(`/${pokemonName}/list`);
        expect(pokemons.body).toEqual([]);
    });
});

describe("Pruebas de la API POKEMON BY NAME", () => {
    it('Response OK: La api debe existir y responder 200', async () => {
        const pokemonName = "pikachu";
        const pokemons = await request(server).get(`/${pokemonName}/data`);
        expect(pokemons.status).toEqual(200);
    });

    it('Imagen default: La api debe devolver la imagen front del pokemon', async () => {
        const pokemonName = "pikachu";
        const pokemons = await request(server).get(`/${pokemonName}/data`);
        expect(pokemons.body.sprites.front_default).toEqual("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/25.png");
    });
});