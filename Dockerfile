FROM node:12-alpine

RUN apk add --no-cache --update krb5-dev alpine-sdk python
RUN npm install -g typescript

RUN mkdir -p /opt/pokemon
WORKDIR /opt/pokemon

COPY package.json ./
RUN npm install --quiet
RUN npm rebuild bcrypt --build-from-source
COPY . .
EXPOSE 80

CMD [ "node", "dist/index.js" ]