import app from './app';
const port = process.env.PORT || 4000;

const server = app
.listen(port, async () => {
    console.log(`Server corriendo sobre el puerto: ${port}`);
})
.on("error", err => {
    console.log(err);
});

export default server;