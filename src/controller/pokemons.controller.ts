import {Request, Response} from 'express';
const fetch = require('node-fetch');

export class PokemonController {
    private _pokemons: Object[];
    private _url: string;
    constructor() {
        this._url = 'https://pokeapi.co/api/v2';
        this._pokemons = [];
    }

    public async init() {
        try {
            let response = await fetch(`${this._url}/pokemon/?offset=0&limit=964`)
            let pokemons = await response.json();
            this._pokemons = pokemons.results;
        } catch(err) {
            console.log(err);
        }
    }

    public async pokemonsByName(req: Request, res: Response) {
        try {
            if(!req.params.name) {
                return res.status(404).send("El nombre del pokemon es obligatorio");
            }
            let pokemonsFilter: Object[] = this.filterByName(req.params.name);
            return res.send(pokemonsFilter);
        } catch (err) {
            return res.status(404).send(err.message);
        }
    }

    public async getPokemonData(req: Request, res: Response): Promise<Object> {
        try {
            if(!req.params.name) {
                return res.status(404).send("El nombre del pokemon es obligatorio");
            }
            let pokemon = await fetch(`${this._url}/pokemon-form/${req.params.name}`);
            let data = await pokemon.json();
            return res.send(data);
        } catch(err) {
            return res.status(404).send(err.message);
        }
    }

    private filterByName(name: string): Object[] {
        return this._pokemons.filter((pokemon:any) => pokemon.name.toLowerCase().includes(name.toLowerCase()));
    }
}