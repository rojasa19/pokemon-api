"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const pokemons_controller_1 = require("../controller/pokemons.controller");
const router = express_1.Router();
const pokemon = new pokemons_controller_1.PokemonController();
pokemon.init();
router.get('/:name/list', pokemon.pokemonsByName.bind(pokemon));
router.get('/:name/data', pokemon.getPokemonData.bind(pokemon));
exports.default = router;
//# sourceMappingURL=index.js.map